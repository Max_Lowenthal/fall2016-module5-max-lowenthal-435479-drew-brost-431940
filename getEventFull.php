 <?php
	
	//gets all the usual stuff and sets it up
	ini_set("session.cookie_httponly", 1);
	session_start();
	require 'database.php';
	if ($_POST['token'] !== $_SESSION['token']){
		die("Request forgery detected");
	}
	
	//gets the Post Id for the post in question
	$id = $_POST['postId'];
	
	//gets us all the info for the post we are trying to learn about
	$check = $mysqli->prepare("select dateTime, description, userId, category, name from meetings where postId =?");
	
		//the following if statements just look for breaks in the mysqli process each time and returns failure if they break
		if(!$check->bind_param('i', $id)){
		
		echo json_encode(array(
		"success" => false,
		"message" => "failed bind"
		));
		exit;
		}
		
		if(!$check->execute()){
			echo json_encode(array(
			"success" => false,
			"message" => "Couldn't execute"
			));
			exit;
		}
		//bind variables
		$check->bind_result($timeD, $description, $userId, $category, $name);
		
		//creates an answer array
		$ans = array();
		
		//if the fetch process works
		if ($check ->fetch()) {
		
		//stop the mysqli
		$check -> close();
		
		//this is just a quick query from the database that gets us the username of the user who created the post, we display this in the info section of the post
		$check2 = $mysqli->prepare("select username from Users where userId = ?");
		$check2->bind_param('i',$userId);
		$check2->execute();
		$check2->bind_result($username);
		$check2->fetch();
		$check2->close();
	
	

		//populates our ans array with all the values we just got from the database!
		$ans["success"] = true;
		$ans["time"] = $timeD;
		$ans["description"] = $description;
		$ans["name"] = $name;
		$ans["userId"] = $userId; 
		$ans["category"] = $category;
		$ans["username"] = $username;
	
		//Sends the JS our whole ans array to work with
		echo json_encode($ans);
		exit; 
		}
		
		//usually failure type stuff
		else {
			echo json_encode(array(
			"success" => false,
			"message" => dateStr
			));
			exit;
		}
	
?>