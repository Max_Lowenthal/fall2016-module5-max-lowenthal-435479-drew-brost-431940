 <?php
	//basic php requirements
	ini_set("session.cookie_httponly", 1);
	session_start();
	require 'database.php';
	
	if ($_POST['token'] !== $_SESSION['token']){
		die("Request forgery detected");
	}
	
	//gets the Post Id for the post you want to delete
	$postId = $_POST['postId'];
	
	//deletes the post from the database
	$check = $mysqli->prepare("delete from meetings where postId=?");
	
	if (!check) {
		echo json_encode(array(
		"success" => false,
	"message" => "failed prep"
	));
	exit;
	}	
	
	if(!$check->bind_param('i', $postId)){
		echo json_encode(array(
		"success" => false,
	"message" => "failed bind"
	));
	exit;
	}
	
	if(!$check->execute()){
		$msg = "Couldn't execute";// + $mysqli-> error;
		echo json_encode(array(
		"success" => false,
	"message" => "deleted"
	));
	exit;
	}
	
	echo json_encode(array(
	"success" => true,
	"message" => "deleted"
	));
	exit;

	
?>