// Bind the buttons to listeners and run startup fcn
document.getElementById("leftArrow").addEventListener("click", decreaseMonth, false); 
document.getElementById("rightArrow").addEventListener("click", increaseMonth, false);
document.addEventListener('DOMContentLoaded', startup, false);
document.getElementById("skip").addEventListener("click", jumper, false);
document.getElementById("jump").addEventListener("click",jumpHome,false);

//set global variables
var monthNumber = 0;
var yearNumber = 2016;
var table = document.getElementById("table");
var tableLabel = table.createCaption();
var categorySet = false;
var categoryGlobal = "none";
var token = "tokeninit";

//startup function
function startup(event) {
	tokenGen();
	setMonthAndYearDefault(event);
	calCreate(event);
	loggedIn();
}

//generates the token before running any other functions
function tokenGen() {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST","token.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			token = jsonData.token;
	}, false); // Bind the callback to the load event
	xmlHttp.send();
}
//creates forms and takes information to jump to a certain date
function jumper(event){
	var x = document.getElementById("skipper");

		var heading = document.createElement('h2'); // Heading of Form
		heading.innerHTML = "Jump to Time";
		x.appendChild(heading);


		
		var name = document.createElement('label'); // Create Label for Date
		name.innerHTML = "Year: ";
		x.appendChild(name);

		var nameChange = document.createElement('input'); // Create Input Field for Date
		nameChange.setAttribute("type", "text");
		nameChange.setAttribute("id", "year1");
		x.appendChild(nameChange);
		
		var linebreak = document.createElement('br');
		x.appendChild(linebreak);

		var timeT = document.createElement('label'); // Create Label for Name Field
		timeT.innerHTML = "Month (in number form please): "; // Set Field Labels
		x.appendChild(timeT);
		
		var nameChange1 = document.createElement('input'); // Create Input Field for Date
		nameChange1.setAttribute("type", "text");
		nameChange1.setAttribute("id", "month1");
		x.appendChild(nameChange1);
		
		var linebreak1 = document.createElement('br');
		x.appendChild(linebreak1);
		
		var submitelement = document.createElement('button'); // Append Submit Button
		submitelement.setAttribute("id", "jump_btn");
		submitelement.setAttribute("value", "Time Warp!");
		submitelement.innerHTML = "Time Warp!";
		x.appendChild(submitelement);
		 document.getElementById("jump_btn").addEventListener("click", function() { timeWarp(document.getElementById("year1").value,document.getElementById("month1").value); }, false);
		

}
//check valid enter date; jump to it
function timeWarp(event, year, month){
	var year2 = document.getElementById("year1").value; 
	var month2 = document.getElementById("month1").value; 
	
	if (month2 > 12 || month2 < 1){
		window.alert("you didn't enter a valid month!");
	}
	else if (year2 <= 0 || year2 > 9999){
		window.alert("you didn't enter a valid year!");
	}
	
	else{
		month2 = month2 - 1;
		monthNumber = month2;
		yearNumber = year2;
	}
	calCreate();
	var x = document.getElementById("skipper");
	x.innerHTML = "";
}

//runs to check to see if a user is logged in, takes actions accordingly
function loggedIn(event){
	//calls a loggedIn PHP to see if someone is loggedin
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST","loggedIn.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			document.getElementById("form_sample").innerHTML = (jsonData.message + "<br>");
			
			//makes a createEvent button if signed in
			var x = document.getElementById("form_sample");
			var newBtn = document.createElement('button');
			newBtn.setAttribute("id","btn1");
			newBtn.setAttribute("onclick","whichButton(this)");
			newBtn.setAttribute("class","w3-btn w3-green w3-text-shadow w3-border w3-ripple w3-border-black");
			newBtn.innerHTML = "Create Event";
			x.appendChild(newBtn);
			
			//log out button
			var logOutBtn = document.createElement('button');
			logOutBtn.setAttribute("id","logOutButt");
			logOutBtn.setAttribute("class","w3-btn w3-red w3-border w3-border-black w3-ripple w3-text-shadow");
			logOutBtn.innerHTML = "Logout";
			x.appendChild(logOutBtn);
			
			//category options
			createCatBtn();
			//creates pass reset stuff
			createResetPass();

			
			document.getElementById("logOutButt").addEventListener("click", loggedOut, false); 

		}else{
			//username text box
			var x1 = document.getElementById("form_sample");
			var userN = document.createElement('input');
			userN.setAttribute("type", "text");
			userN.setAttribute("id","username");
			userN.setAttribute("placeholder","Username");
			x1.appendChild(userN);
			
			//password text box
			var passW = document.createElement('input');
			passW.setAttribute("type","password");
			passW.setAttribute("id","password");
			passW.setAttribute("placeholder","Password");
			x1.appendChild(passW);
			
			//log in button
			var logBtn = document.createElement('button');
			logBtn.setAttribute("id","login_btn");
			logBtn.setAttribute("value","Log In");
			logBtn.innerHTML = "Log In";
			logBtn.setAttribute("class","w3-btn w3-red w3-text-shadow w3-border w3-border-black w3-ripple");
			x1.appendChild(logBtn);
			
			//create new user button
			var newBtn1 = document.createElement('button');
			newBtn1.setAttribute("id","new_user");
			newBtn1.setAttribute("value","New User");
			newBtn1.innerHTML = "New User";
			newBtn1.setAttribute("class","w3-btn w3-red w3-text-shadow w3-border w3-border-black w3-ripple");
			x1.appendChild(newBtn1);
			
			
			
			document.getElementById("login_btn").addEventListener("click", userAjax, false); // Bind the AJAX call to button click
			document.getElementById("new_user").addEventListener("click", newUserAjax, false); 
			//clear out category selection and options
			categoryGlobal = "none";
			var x2 = document.getElementById("cat_change");
			while(x2.firstChild){
				x2.removeChild(x2.firstChild);
			}
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(); // Send the data
	calCreate();
}
//creates a password reset button and box CREATIVE
function createResetPass() {
	var x = document.getElementById("changePass");
	
	while(x.firstChild){
		x.removeChild(x.firstChild);
	}
	
	var newP = document.createElement('input');
	newP.setAttribute("type", "text");
	newP.setAttribute("id","passToBe");
	newP.setAttribute("placeholder","new password");
	x.appendChild(newP);
			
	var passBtn = document.createElement('button');
	passBtn.setAttribute("id","passBtn");
	passBtn.setAttribute("class","w3-btn w3-orange w3-ripple w3-border w3-border-black");
	passBtn.innerHTML = "Reset Password";
	x.appendChild(passBtn);
	
	
	document.getElementById("passBtn").addEventListener("click", function() { passSet(document.getElementById("passToBe").value); }, false);
}

//sets a password to the given value CREATIVE
function passSet(newPass) {
	var dataString = "newPass=" + encodeURIComponent(newPass) + "&token=" + encodeURIComponent(token);
		var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
		xmlHttp.open("POST", "passReset.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
		xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
		xmlHttp.addEventListener("load", function(event){
			var jsonData = JSON.parse(event.target.responseText); 
			if(jsonData.success){  
				//window.alert("says pass is changed" + jsonData.message);
			}else{
				window.alert("no date found. Forever alone... because "+jsonData.message);
			}
		}, false); // Bind the callback to the load event
		xmlHttp.send(dataString);
}

//creates category options and button CREATIVE
function createCatBtn() {
	//window.alert("Found the thing");
	var x = document.getElementById("cat_change");
	
	while(x.firstChild){
		x.removeChild(x.firstChild);
	}
	
	var createform = document.createElement('form');
	var inputCategories = document.createElement('select'); // Create Input Field for Categories
		inputCategories.setAttribute("type", "select");
		inputCategories.setAttribute("id", "category");
		//inputCategories.class=class="searchw8";
		createform.appendChild(inputCategories);
		
		//creates options in dropdown for categories
		
		var option0 = document.createElement('option');
		option0.value="none";
		option0.innerHTML= "none";
		inputCategories.appendChild(option0);
		
		var option1 = document.createElement('option');
		option1.value="meeting";
		option1.innerHTML= "Meeting";
		inputCategories.appendChild(option1);
		
		var option2 = document.createElement('option');
		option2.value="FUN";
		option2.innerHTML= "FUN";
		inputCategories.appendChild(option2);
		
		var option3 = document.createElement('option');
		option3.value="friends";
		option3.innerHTML= "friends";
		inputCategories.appendChild(option3);
		
		var option4 = document.createElement('option');
		option4.value="sports";
		option4.innerHTML= "sports";
		inputCategories.appendChild(option4);
		
		var option5 = document.createElement('option');
		option5.value="whimsy";
		option5.innerHTML= "whimsy";
		inputCategories.appendChild(option5);
		
		var option6 = document.createElement('option');
		option6.value="flow";
		option6.innerHTML= "flow";
		inputCategories.appendChild(option6);
	
	
	var catBtn = document.createElement('button');
	catBtn.setAttribute("id","catBtn");
	catBtn.setAttribute("class", "w3-btn w3-cyan w3-border w3-border-black w3-ripple");
	catBtn.innerHTML = "Filter by Category";
	
	x.appendChild(inputCategories);
	x.appendChild(catBtn);
	
	document.getElementById("catBtn").addEventListener("click", function() { catSet(document.getElementById("category").value); }, false);
}

//sets the category and updates
function catSet(cat) {
	
	categoryGlobal = cat;
	if (cat != "none") {
		categorySet = true;
		//window.alert(cat);
	}
	else {
		categorySet = false;
	}
	calCreate();
}
			
//creates calendar (sets main table and calls for each entry)
//code helped by http://stackoverflow.com/questions/14643617/create-table-using-javascript
function calCreate(event) {
	var x = document.getElementById("event_info");
	x.innerHTML = "";
	
	//Clear table
	while(table.rows.length) {
		table.deleteRow(0);
	}
	// Create an empty <thead> element and add it to the table:
	var header = table.createTHead();
	var row = header.insertRow(0);   
//Sets labels for each day
	var cell = row.insertCell(0);
	cell.innerHTML = "<b>Sunday</b>";
	var cell1 = row.insertCell(1);
	cell1.innerHTML = "<b>Monday</b>";
	cell1.setAttribute("class","w3-container w3-centered");
	var cell2 = row.insertCell(2);
	cell2.innerHTML = "<b>Tuesday</b>";
	cell2.setAttribute("class","w3-container w3-centered");
	var cell3 = row.insertCell(3);
	cell3.innerHTML = "<b>Wednesday</b>";
	cell3.setAttribute("class","w3-container w3-centered");
	var cell4 = row.insertCell(4);
	cell4.innerHTML = "<b>Thursday</b>";
	cell4.setAttribute("class","w3-container w3-centered");
	var cell5 = row.insertCell(5);
	cell5.innerHTML = "<b>Friday</b>";
	cell5.setAttribute("class","w3-container w3-centered");
	var cell6 = row.insertCell(6);
	cell6.innerHTML = "<b>Saturday</b>";
	cell6.setAttribute("class","w3-container w3-centered");
	
	//sets table label to month name and year
	tableLabel.innerHTML = "<b><h1>" + monthToName(monthNumber) + " " + yearNumber + "</h1></b>";
	tableLabel.setAttribute("class","w3-indigo");
	var month = monthNumber;
	var year = yearNumber;
	var day = daysInMonth(year,month);

	//get number of weeks
	var lastWeek = weeksInMonth(year, month);
	var body = document.body;

	//FIXME
	table.style.border = '3px solid black';
	//creates arrays of rows and a cal object
	var row3 = [];
	var cal;
	
	//for each week in the month...
	for(var i=0; i<lastWeek; i++) {
		//makes a row...
		row3.push(table.insertRow());
		//and fills it
		for (var j=0; j<7; j++) {
				cal = row3[i].insertCell();
				createCalEntry(cal, year, month, day, i, j);
				cal.style.border = '3px solid black';

		}
	}

	body.appendChild(table);
}

//creates a cell of the calendar
function createCalEntry(target, year, month, day, i, j) {
	var firstDay = firstDayMonth(year, month);
	var monthLength = daysInMonth(year, month);
	var currentDay = 7*i + j - firstDay;
	

	//if it's a day of the current month (not before or after it starts)
	if ((j>= firstDay || i>0) && currentDay < monthLength) {
		//adds day number
		target.appendChild(document.createTextNode(currentDay + 1));
		var dataString = "day=" + encodeURIComponent(currentDay)+"&month=" + encodeURIComponent(month)+"&year=" + encodeURIComponent(year) + "&token=" + encodeURIComponent(token);
		var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
		xmlHttp.open("POST", "getEventSimple.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
		xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
		xmlHttp.addEventListener("load", function(event){
			var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
			
			if(jsonData.success){  
			//pulls all events for each day, and gets attributes, creates a button
			for (var alpha in jsonData.names) {
				if (!categorySet || jsonData.category[alpha] == categoryGlobal) {
					var linebreak = document.createElement('br');
					target.appendChild(linebreak);
					var safeName = jsonData.names[alpha]; //this was escaped when it was run through the post method; should not be double escaped
					target.appendChild(document.createTextNode( "Event:" + safeName));
					var btn = document.createElement('input');
					btn.type = "button";
					btn.setAttribute("id","event_btn");
					btn.setAttribute("value","More Info!");
					var a = jsonData.postId[alpha];
					//executes getDataFromEvent(a) on click
					btn.setAttribute("onclick","getDataFromEvent(" + a + ")");
					target.appendChild(btn);
				}
			}
			}else{
				window.alert("Date error, because "+jsonData.message);
			}
		}, false); // Bind the callback to the load event
		xmlHttp.send(dataString); // Send the data
	} else {
		//fill empty squares
		target.appendChild(document.createTextNode("        "));
	}
	return null;
}

//gets more details from events, and creates event-specific options
function getDataFromEvent(postId) {
	//gets event info
	var time, description, userId, category, name;
	var dataString = "postId=" + encodeURIComponent(postId)+ "&token=" + encodeURIComponent(token);
		var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
		xmlHttp.open("POST", "getEventFull.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
		xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
		xmlHttp.addEventListener("load", function(event){
			var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
			//window.alert("it's loaded!");
			if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			//window.alert("You've found a date!");
			time = jsonData.time;
			description = jsonData.description;
			userId = jsonData.userId; //this should be username - another sql call
			category = jsonData.category;
			name = jsonData.name;
			var username = jsonData.username;
			var x = document.getElementById("event_info");
			
			x.innerHTML = ""; //clear the div
			
				//creates event info section
			var heading = document.createElement('h2'); // Heading of Form
			var title = document.createTextNode("Event Info ");
			heading.appendChild(title);
			
			var timeP = document.createElement('p');
			var timeT = document.createTextNode("Time of Event: " + time);
			timeP.appendChild(timeT);
			
			var nameP = document.createElement('p');
			var nameT = document.createTextNode("Event Name: " + name);
			nameP.appendChild(nameT);
			
			var discP = document.createElement('p');
			var discT = document.createTextNode("Description: " + description);
			discP.appendChild(discT);
			
			var userP = document.createElement('p');
			var userT = document.createTextNode("Creator: " + username); 
			userP.appendChild(userT);
			
			var categoryP = document.createElement('p');
			var categoryT = document.createTextNode("Category: " +category);
			categoryP.appendChild(categoryT);
						
			var linebreak = document.createElement('br');

			
			x.appendChild(nameP);
			x.appendChild(linebreak);
			x.appendChild(timeP);
			x.appendChild(linebreak);
			x.appendChild(discP);
			x.appendChild(linebreak);
			x.appendChild(categoryP);
			x.appendChild(linebreak);
			x.appendChild(userP);
			x.appendChild(linebreak);
			
			//delete an event
			var delBtn = document.createElement('button');
			delBtn.setAttribute("id","del_btn");
			delBtn.setAttribute("value","Delete Event");
			delBtn.innerHTML = "Delete Event";
			x.appendChild(delBtn);
			
			//modify button
			var modBtn = document.createElement('button');
			modBtn.setAttribute("id","mod_btn");
			modBtn.setAttribute("value","Modify Event");
			modBtn.innerHTML = "Modify Event";
			x.appendChild(modBtn);
			
			//create sharing form and button
			var shareBtn = document.createElement('button');
			shareBtn.setAttribute("id","share_btn");
			shareBtn.setAttribute("value","Share Event");
			shareBtn.innerHTML = "Share event";
			x.appendChild(shareBtn);
			
			//create field for user to share with
			var namelabel = document.createElement('label'); // Create Label for Name Field
			namelabel.innerHTML = "Share with : "; // Set Field Labels
			x.appendChild(namelabel);
			
			//create input field for user to share with
			var inputDescrip= document.createElement('input'); // Create Input Field for Description
			inputDescrip.setAttribute("type", "text");
			inputDescrip.setAttribute("id", "shareWith");
			x.appendChild(inputDescrip);
		
			//add functionality to share button delete and modify buttons
			document.getElementById("share_btn").addEventListener("click", function() { shareEvent(postId, document.getElementById("shareWith").value); }, false);
			
			document.getElementById("del_btn").addEventListener("click", function() { deleteEvent(postId); }, false);
			
			document.getElementById("mod_btn").addEventListener("click", function() { modifyEvent(name, time, description, category, postId); }, false);
			
			}else{
				window.alert("no date found, because "+jsonData.message);
			}
		}, false); // Bind the callback to the load event
		xmlHttp.send(dataString);
}

//runs the delete PHP script and deletes an event from the database
function deleteEvent(postId) {

	// Make a URL-encoded string for passing POST data:
	var dataString = "postId=" + encodeURIComponent(postId)+ "&token=" + encodeURIComponent(token);
	
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "delete_event.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			window.alert("Event deleted!");
		}else{
			window.alert("Event not deleted. " + jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString);
	calCreate();
}

//creates functionality to share events with the user based on the username in input field created above 
function shareEvent(postId, shareUserName) {
	
	//this does the usual PHP call with the relavent information
	var dataString = "postId=" + encodeURIComponent(postId) + "&username=" + encodeURIComponent(shareUserName)+ "&token=" + encodeURIComponent(token);
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "share_event.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			window.alert("Event shared! ");
		}else{
			window.alert("Event not shared. "+ jsonData.message );
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString);
	calCreate();
	
	
}

//creates the fields for event modifaction and populates them with event data
function modifyEvent(name,time,description, category, postId) {
	//makes the name and description readable by the JS
	
	var y = "" + name;
	var z = "" + description;

	//clear the div
	var x = document.getElementById("event_info");
	x.innerHTML = ""; 
	
	var createform = document.createElement('form'); // Create New Element Form
		createform.setAttribute("action", ""); // Setting Action Attribute on Form
		createform.setAttribute("method", "post"); // Setting Method Attribute on Form
		x.appendChild(createform);

		var heading = document.createElement('h2'); // Heading of Form
		heading.innerHTML = "Modify Event";
		createform.appendChild(heading);

		//linebreak
		var linebreak = document.createElement('br');
		createform.appendChild(linebreak);
		
		var name4 = document.createElement('label'); // Create Label for Event Name
		name4.innerHTML = "Name: ";
		createform.appendChild(name4);

		var nameChange = document.createElement('input'); // Create Input Field for Name
		nameChange.setAttribute("type", "text");
		nameChange.setAttribute("id", "name1");
		nameChange.setAttribute("value", y);
		createform.appendChild(nameChange);
		
		//linebreak
		var linebreak6 = document.createElement('br');
		createform.appendChild(linebreak6);

		var timeT = document.createElement('label'); // Create Label for Time
		timeT.innerHTML = "Event Time and Date : "; // Set Field Labels
		createform.appendChild(timeT);

		var timeTValue = document.createElement('input'); // Create Input Field for DateTime
		timeTValue.setAttribute("type", "datetime-local");
		timeTValue.setAttribute("id", "dateAndTime1");
		createform.appendChild(timeTValue);
		
		//linebreak
		var linebreak7 = document.createElement('br');
		createform.appendChild(linebreak7);
		
		var descrip = document.createElement('label'); // Create Label for Description
		descrip.innerHTML = "Description : "; // Set Field Labels
		createform.appendChild(descrip);

		var inputDescrip= document.createElement('input'); // Create Input Field for Description
		inputDescrip.setAttribute("type", "text");
		inputDescrip.setAttribute("id", "descrip1");
		inputDescrip.setAttribute("value", z);
		createform.appendChild(inputDescrip);
		
		//linebreak
		var linebreak8 = document.createElement('br');
		createform.appendChild(linebreak8);

		var categories = document.createElement('label'); // Create Label for Categories
		categories.innerHTML = "Categories: ";
		createform.appendChild(categories);

		var inputCategories = document.createElement('select'); // Create Input Field for Categories
		inputCategories.setAttribute("type", "select");
		inputCategories.setAttribute("id", "category1");
		createform.appendChild(inputCategories);
		
		
		//creates options in dropdown for categories
		var option = document.createElement('option');
		option.value=category;
		option.innerHTML= category;
		inputCategories.appendChild(option);
		
		var option1 = document.createElement('option');
		option1.value="meeting";
		option1.innerHTML= "Meeting";
		inputCategories.appendChild(option1);
		
		var option2 = document.createElement('option');
		option2.value="FUN";
		option2.innerHTML= "FUN";
		inputCategories.appendChild(option2);
		
		var option3 = document.createElement('option');
		option3.value="friends";
		option3.innerHTML= "friends";
		inputCategories.appendChild(option3);
		
		var option4 = document.createElement('option');
		option4.value="sports";
		option4.innerHTML= "sports";
		inputCategories.appendChild(option4);
		
		var option5 = document.createElement('option');
		option5.value="whimsy";
		option5.innerHTML= "whimsy";
		inputCategories.appendChild(option5);
		
		var option6 = document.createElement('option');
		option6.value="flow";
		option6.innerHTML= "flow";
		inputCategories.appendChild(option6);

		var messagebreak = document.createElement('br');
		createform.appendChild(messagebreak);
		
		var submitelement = document.createElement('input'); // Append Submit Button
		submitelement.setAttribute("type", "submit");
		submitelement.setAttribute("id", "mod_btn");
		submitelement.setAttribute("value", "Modify");
		createform.appendChild(submitelement);
		 document.getElementById("mod_btn").addEventListener("click", function() { modEvent(postId); }, false);
	
}

//actually runs the PHP that changes the event info
function modEvent(postId){
	event.preventDefault();
	//pulls in all the new modified info
	var eventName = document.getElementById("name1").value; 
	var dateT = document.getElementById("dateAndTime1").value; 
	var description = document.getElementById("descrip1").value; 
	var category = document.getElementById("category1").value; 
	
	var dataString = "postId=" + encodeURIComponent(postId)+"&name1=" + encodeURIComponent(eventName)+"&dateAndTime1="+encodeURIComponent(dateT)+"&descrip1="+encodeURIComponent(description)+"&category1="+encodeURIComponent(category) + "&token=" + encodeURIComponent(token);
	
	
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "modify_event.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			window.alert("Event modified!");
		}else{
			window.alert("Event not modified." + jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString);
	calCreate();

}
//creates functionality to jump back to the current month
function jumpHome(event){
	setMonthAndYearDefault();
	calCreate();
}

//sets the default current month and year value
function setMonthAndYearDefault(event) {
	
	var date = new Date();
	var defaultMonth = date.getMonth();
	var defaultYear = date.getFullYear();
	 
	monthNumber=defaultMonth;
	yearNumber = defaultYear;
}

//functionality for moving forward in months
function increaseMonth() {
	var year = yearNumber;
	var month = monthNumber;
	
	month = month+1;
	if (month >= 12) {
		year++;
		month =month -12;
	}
	monthNumber = month;
	yearNumber = year;
	
	calCreate();
}

//functionality for moving backwards in months
function decreaseMonth() {
	var year = yearNumber;
	var month = monthNumber;
	
	month = month-1;
	if (month < 0) {
		year--;
		month=month +12;
	}
	monthNumber = month;
	yearNumber = year;
	calCreate();
}


//this is all stuff for figuring out the calendar layout
//------------------------------------------------------------------------

//figures out the first weekday of a month given the year
function firstDayMonth(year, month) {
	var firstDayOfMonth = new Date(year, month, 1);
	return firstDayOfMonth.getDay();
}

//figures out the month length for a month given the year
function daysInMonth(year, month) {
	var d= new Date(year, month+1, 0);
    return d.getDate();
}

//figures out the layout of weeks in a month
function weeksInMonth(year, month) {
	var length = daysInMonth(year, month);
	var start = firstDayMonth(year, month); 
	var ans = Math.floor((length + start + 7 - 1)/7); //This is a cute arithmetic way to round up! Look into it, it's sweet (not my invention) http://stackoverflow.com/questions/7446710/how-to-round-up-integer-division-and-have-int-result-in-java
	return ans;
}

//calendar figuring out stuff ends here
//----------------------------------------------------------------------------------


//functionality for logging out
function loggedOut(event){
	var xmlHttp = new XMLHttpRequest();
	//var dataString ="token=" + encodeURIComponent(token);
	xmlHttp.open("POST","loggedOut.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){
			document.getElementById("form_sample").innerHTML = (jsonData.message + "<br>");
			loggedIn();
		}
		else{
			window.alert("You're not logged In, but somehow you got here!");
		}
	},false);
	xmlHttp.send(); // Send the data
	
	//if you logg out you dont get the option to change categories or password anymore
	var x = document.getElementById("cat_change");
	while(x.firstChild){
		x.removeChild(x.firstChild);
	}
	var y = document.getElementById("changePass");
	while(y.firstChild){
		y.removeChild(y.firstChild);
	}
	catSet("none");
}

//functionality for users that already exist
function userAjax(event){

	var username = document.getElementById('username').value; // Get the username from the form
	var password = document.getElementById('password').value; // Get the password from the form

	// Make a URL-encoded string for passing POST data:
	var dataString = "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password) + "&token=" + encodeURIComponent(token);
	
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "login_ajax.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			//window.alert("You've been Logged In!" + jsonData.message);
			
			loggedIn();
			
		}else{
			window.alert("You weren't logged In" + jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
}

//functionality for when you create a new user
function newUserAjax(event){
	var username = document.getElementById("username").value; // Get the username from the form
	var password = document.getElementById("password").value; // Get the password from the form
 
	// Make a URL-encoded string for passing POST data:
	var dataString = "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password) + "&token=" + encodeURIComponent(token);
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "newUser_ajax.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			//window.alert("You've created a User!");
			document.getElementById("form_sample").innerHTML = jsonData.message;
			
			loggedIn();
		}else{
			window.alert("This username is taken. "+jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
	
}  

//creates the form for creating an event, only if someone clicks the create event button
function whichButton(buttonElement){
  var buttonClickedId = buttonElement.id;
  if( buttonClickedId === 'btn1' ){
	  
	  //makes the form for event creation and all its fields
		var x = document.getElementById("form_sample2");
		var createform = document.createElement('form'); // Create New Element Form
		createform.setAttribute("action", ""); // Setting Action Attribute on Form
		createform.setAttribute("method", "post"); // Setting Method Attribute on Form
		x.appendChild(createform);

		var heading = document.createElement('h2'); // Heading of Form
		heading.innerHTML = "Create Event";
		createform.appendChild(heading);

		//linebreak
		var linebreak = document.createElement('br');
		createform.appendChild(linebreak);
		
		var date = document.createElement('label'); // Create Label for Date
		date.innerHTML = "Date and Time: ";
		createform.appendChild(date);

		var dateInput = document.createElement('input'); // Create Input Field for Date
		dateInput.setAttribute("type", "datetime-local");
		dateInput.setAttribute("id", "date");
		createform.appendChild(dateInput);
		
		//linebreak
		var linebreak11 = document.createElement('br');
		createform.appendChild(linebreak11);

		
		var namelabel = document.createElement('label'); // Create Label for Name Field
		namelabel.innerHTML = "Event Name : "; // Set Field Labels
		createform.appendChild(namelabel);

		var inputName = document.createElement('input'); // Create Input Field for Name
		inputName.setAttribute("type", "text");
		inputName.setAttribute("id", "event_name");
		createform.appendChild(inputName);

		//linebreak
		var linebreak12 = document.createElement('br');
		createform.appendChild(linebreak12);
		
		var descrip = document.createElement('label'); // Create Label for Description
		descrip.innerHTML = "Description : "; // Set Field Labels
		createform.appendChild(descrip);

		var inputDescrip= document.createElement('input'); // Create Input Field for Description
		inputDescrip.setAttribute("type", "text");
		inputDescrip.setAttribute("id", "descrip");
		createform.appendChild(inputDescrip);

		//linebreak
		var linebreak13 = document.createElement('br');
		createform.appendChild(linebreak13);

		var categories = document.createElement('label'); // Create Label for Categories
		categories.innerHTML = "Categories: ";
		createform.appendChild(categories);

		var inputCategories = document.createElement('select'); // Create Input Field for Categories
		inputCategories.setAttribute("type", "select");
		inputCategories.setAttribute("id", "category");
		createform.appendChild(inputCategories);
		
		//creates options in dropdown for categories
		
		var option1 = document.createElement('option');
		option1.value="meeting";
		option1.innerHTML= "Meeting";
		inputCategories.appendChild(option1);
		
		var option2 = document.createElement('option');
		option2.value="FUN";
		option2.innerHTML= "FUN";
		inputCategories.appendChild(option2);
		
		var option3 = document.createElement('option');
		option3.value="friends";
		option3.innerHTML= "friends";
		inputCategories.appendChild(option3);
		
		var option4 = document.createElement('option');
		option4.value="sports";
		option4.innerHTML= "sports";
		inputCategories.appendChild(option4);
		
		var option5 = document.createElement('option');
		option5.value="whimsy";
		option5.innerHTML= "whimsy";
		inputCategories.appendChild(option5);
		
		var option6 = document.createElement('option');
		option6.value="flow";
		option6.innerHTML= "flow";
		inputCategories.appendChild(option6);

		//linebreak
		var messagebreak = document.createElement('br');
		createform.appendChild(messagebreak);
		
		var submitelement = document.createElement('input'); // Append Submit Button
		submitelement.setAttribute("type", "submit");
		submitelement.setAttribute("id", "create_btn");
		submitelement.setAttribute("value", "Submit");
		createform.appendChild(submitelement);
		 document.getElementById("create_btn").addEventListener("click", createEvent, false);
  }
  
  else{
     // don't know which button was clicked
  }
  
  //adds the actual functionality to create an event
  function createEvent(){
	  
	//STOPS THE REFRESHING
	event.preventDefault(); 
	
	//Pulls in the variables to be created
	var dateT = document.getElementById("date").value; 
	var eventName = document.getElementById("event_name").value; 
	var description = document.getElementById("descrip").value; 
	var category = document.getElementById("category").value; 
	var x = document.getElementById("form_sample2");

	// Make a URL-encoded string for passing POST data:
	var dataString = "dateT=" + encodeURIComponent(dateT) + "&eventName=" + encodeURIComponent(eventName) + "&description=" + encodeURIComponent(description)+ "&category=" + encodeURIComponent(category) + "&token=" + encodeURIComponent(token);

	
	var xmlHttp = new XMLHttpRequest(); // Initialize our XMLHttpRequest instance
	xmlHttp.open("POST", "createEvent_ajax.php", true); // Starting a POST request (NEVER send passwords as GET variables!!!)
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); // It's easy to forget this line for POST requests
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText); // parse the JSON into a JavaScript object
		if(jsonData.success){  // in PHP, this was the "success" key in the associative array; in JavaScript, it's the .success property of jsonData
			window.alert("Event been Added!!");
		}else{
			window.alert("Event wasn't added. " + jsonData.message);
		}
	}, false); // Bind the callback to the load event
	xmlHttp.send(dataString); // Send the data
	  x.innerHTML= "";
	  calCreate();
  }
  

}

//translate the array of months stored as numbers into names
function monthToName(month) {
	var monthNames = ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","December"];
	return monthNames[month];
}
