<?php
	ini_set("session.cookie_httponly", 1);
	session_start();
	require 'database.php';
	if ($_POST['token'] !== $_SESSION['token']){
		die("Request forgery detected");
	}
	
	//grabs all our posted and session values
	$dateT = $_POST['dateT'];
	$Name = $_POST['eventName'];
	$descrip = $_POST['description'];
	$category = $_POST['category'];
	$user_id = $_SESSION['userId'];

	
//preps the event info to enter the database, 
$launch = $mysqli->prepare("insert into meetings (description, dateTime, name, category,userId) values (?, ?, ?, ?, ?)");
	if (!$launch) {
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	};
	
	//binds them
	if (!$launch->bind_param('ssssi', $descrip, $dateT, $Name, $category,$user_id)) {
		echo "failed bind";
	}
	$weNotIn = (!$launch->execute());
	$launch->close();
	
	//returns false if process didnt work
	if ($weNotIn) {
		echo json_encode(array(
		"success" => false,
	"message" => "Couldn't add event"
	));
	exit;
	}
	// if the process works send all this info back to the JS
	else {
		echo json_encode(array(
	"success" => true,
	"message" => "Event added!"
	));
	exit;
	}
	?>