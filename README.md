# **WARNING: This code and Module are optimized for Google Chrome. If you use a different browser, you're going to have a problem**
Hi there! Thanks for Grading our Module 5.

Names: Drew Brost and Max Lowenthal
Student IDs: 431940 and 435479
[Here's a link to our homepage!](http://ec2-54-186-115-66.us-west-2.compute.amazonaws.com/~stilkin/module5/sync/boogleCal.php)

Description of Creative Portion
As Kanye West said:

>We're living the future so 
> the present is our past. 

Part 1: **Travel Through Time!** *(Go to a specific Month and Year)*
Description:
	No need to go 88 miles per hour for this one. We've implemented a feature for all users, even those not signed in.
	This feature allows users to enter in a year, and a month (in number form, so September would be 9 and etc.) and travel directly to that month and year!
	This feature is super useful for creating events way far in the future for planning ahead, or remembering old events fondly! 
	I used the feature to store my birthday back in 1995 in one of our users, and this made it alot easier to get to, rather than having to click month by month.

Part 2: **Bring It Back Now Yall** *(Come back to the present month)*
Description:
	We wouldnt want to leave you stranded in the past like Marty McFly, so built a little button to help. This button
	known as the "Return to Current Month" button, brings the user back to the current month they are in when viewing the calendar.
	This is useful for users who like to input plenty of events far in the future, as it helps them navigate back to the present day and see what they have coming up.

Part 3: **Group on Up!** *(Sort events by category tags)*
Description:
	We may not be in high school anymore but that doesn't stop us from having cliques! Our events are no exception,
	as each one is given a category by its creator from our pre-created list of categories when they are created.
	This feature, which is only implemented for our lucky signed in users, allows anyone looking at the calendar to filter
	the view of the current month they are working on by category. That way you can see all the meetings you have, without any
	of those pesky FUN events getting in the way!

Part 4: **Everyone deserves a Second Chance** *(Reset your Password)*
Description:
	Remembering stuff is hard. Maybe you made your password just too tough when you created your account and wanna switch it to something
	a little easier? Well we have just the feature for you (provided you are a signed in user of course)! This box appears for all signed in users
	and allows them to simply enter a new password and reset the password tied to their account. They dont have to sign back in or anything,
	but their password will be magically changed the next time they sign in. (CAUTION: there is no real magic here, unless you count the magic of Computer Science)

Part 5: **Why Can't We Be Friends?** *(Share an event with another User)*
Description:
	Got a fun party coming up? Trying to set up a meal with your long lost pal? Well we've built a feature that can help (only available to those specially selected signed in users)!
	By selecting "More Info" on any event, you will see a text input box that allows you to enter a username. By entering the username of the desired user you wish to share your event with, and hitting the beautiful looking button 
	(Shouts Out to CSS for the beauty) your shared pal will now have the event on their very own calendar when they sign in! (WARNING: We don't want your events getting _TOO_ out of hand, so you can only invite one user a time, you party animal you. 
	You'll have to invite people one by one!)

_Conclusion:_
	We hope you've enjoyed our calendar, and all of its beautiful creative features. We hope your day is going well, and that you're not too bored grading all these Modules. Feel free to take a break, you've earned it TA. 
	
	See you again for Module 6,
	Max and Drew 
