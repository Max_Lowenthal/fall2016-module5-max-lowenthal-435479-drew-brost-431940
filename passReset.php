<?php

	require 'database.php';
	ini_set("session.cookie_httponly", 1);
	session_start();
	if ($_POST['token'] !== $_SESSION['token']){
		die("Request forgery detected");
	}
	
	//pulls in the username and password from the sign boxes
	$userId = $_SESSION['userId'];
	$newPass = $_POST['newPass'];
	

	
	
//updates the password to be entered into the database, then enters it
$launch = $mysqli->prepare("update Users set saltPass = ? where userId = ?");
	if (!$launch) {
		echo json_encode(array(
		"success" => false,
	"message" => "launch fail"
	));
		exit;
	};
	
	if (!$launch->bind_param('si', crypt($newPass), $userId)) {
		echo json_encode(array(
		"success" => false,
	"message" => "bind failed"
	));
	}
	$weNotIn = (!$launch->execute());
	$launch->close();
	
	//returns false if process didnt work
	if ($weNotIn) {
		echo json_encode(array(
		"success" => false,
	"message" => "Couldn't change password"
	));
	exit;
	}
	// if the process works we move back to JS
	else {
		echo json_encode(array(
	"success" => true,
	"message" => $userId
	
	));
	exit;
	}
?>