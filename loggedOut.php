<?php
	//usual business here
	ini_set("session.cookie_httponly", 1);
	session_start();
	//if ($_POST['token'] !== $_SESSION['token']){
	//	die("Request forgery detected");
	//}
	
	//checks if the user is currently signed in, and signs them out
	if(isset($_SESSION['userId'])){
		$_SESSION['userId'] = null;
		echo json_encode(array(
		"success" => true,
		"message" => "You've signed out!"
	));
	exit;
		
	}
	
	//if somehow someone got to log out without being signed in it returns an error
	else{
		echo json_encode(array(
		"success" => false,
	));
	exit;

	}





?>