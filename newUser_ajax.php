<?php

	require 'database.php';
	
	//pulls in the username and password from the sign boxes
	$user = $_POST['username'];
	$password = $_POST['password'];
	
	
	
//preps the username and password to be entered into the database, then enters it
$launch = $mysqli->prepare("insert into Users (username, saltPass) values (?, ?)");
	if (!$launch) {
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	};
	
	if (!$launch->bind_param('ss', $user, crypt($password))) {
		echo "failed bind";
	}
	$weNotIn = (!$launch->execute());
	$launch->close();
	
	//returns false if process didnt work
	if ($weNotIn) {
		$mysqli->error;
		echo json_encode(array(
		"success" => false,
	"message" => "Couldn't create user"
	));
	exit;
	}
	
	// if the process works we store the UserId in a session variable, and send all this info back to the JS
	else {
		ini_set("session.cookie_httponly", 1);
		session_start();
		$_SESSION['username'] = $user;
		//$_SESSION['token'] = substr(md5(rand()), 0, 10);
		//$token = $_SESSION['token'];
		$check = $mysqli->prepare("select userId from Users where username =?");
		//this could be improved
		$check->bind_param('s', $user);
		$check->execute();
	
		$check->bind_result($idnum);
		$check ->fetch();
		$check -> close();
		$idnum = htmlspecialchars($idnum);
		$_SESSION['userId'] = $idnum;
		echo json_encode(array(
	"success" => true,
	//"token" => $token
	));
	exit;
	}
	?>