<?php
	//usual stuff
	require 'database.php';
	ini_set("session.cookie_httponly", 1);
	session_start();
	
	//makes the first token
	$_SESSION['token'] = substr(md5(rand()), 0, 10);
	$token = $_SESSION['token'];
	
	echo json_encode(array(
	"success" => true,
	"token" => $token
	));
	exit;

?>