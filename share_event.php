 <?php
 
	//usual business
	ini_set("session.cookie_httponly", 1);
	session_start();
	require 'database.php';
	$ownerId = $_SESSION['userId'];
	$postId = $_POST['postId'];
	$username = $_POST['username'];
	
	//gets the userId of the person we want to share with
	$check = $mysqli->prepare("select userId from Users where username =?");
	if(!$check->bind_param('s', $username)){
		echo json_encode(array(
		"success" => false,
	"message" => "failed bind"
	));
	exit;
	}
	
	if(!$check->execute()){
		echo json_encode(array(
		"success" => false,
	"message" => "Couldn't execute"
	));
	exit;
	}
	
	//binds the userId of the shared person
	$check->bind_result($sharedWithId);
	
	$check->fetch();
	$check -> close();
	
	if ($sharedWithId == $ownerId) {
		echo json_encode(array(
		"success" => false,
	"message" => "You can't share an event with yourself, silly!"
	));
	exit;
	}
	$check -> close();
	
	//Inserts the UserId of the person we want to share with into our sharing array, which is utilized to figure out the sharing process
	$check = $mysqli->prepare("insert into sharing (ownerId, sharedWithId, postId) values (?,?,?)");
	
	//failure procedures
	if(!$check->bind_param('iii', $ownerId, $sharedWithId, $postId)){
		echo json_encode(array(
		"success" => false,
	"message" => "failed bind"
	));
	exit;
	}

	
	
	if(!$check->execute()){
		$msg = "Couldn't execute";// + $mysqli-> error;
		echo json_encode(array(
		"success" => false,
	"message" => $msg,
	"user" => $username,
	"ownerId" => $ownerId,
	"postId" => $postId
	
	));
	exit;
	}
	
	//success protocol
	echo json_encode(array(
		"success" => true,
	"message" => $msg
	));
	
	$check -> close();
	
	?>