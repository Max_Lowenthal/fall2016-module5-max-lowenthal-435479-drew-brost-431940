 <?php
 
	//usual stuff we need
	ini_set("session.cookie_httponly", 1);
	session_start();
	require 'database.php';

	//gets all the inputed and existing info
	$userId = $_SESSION['userId'];
	$day = $_POST['day']+1;
	$month = $_POST['month']+1;
	$year = $_POST['year'];

	
	//creates some variables for later
	$dateStr = ''.$year.'-'.$month.'-'.$day;
	$name = "ghosts";
	$dateStr = ''.$year.'-'.$month.'-'.$day; 
	$date = $dateStr; 

	//complex query that joins with the sharing table to allow for event sharing
	$check = $mysqli->prepare("select meetings.name, meetings.dateTime, meetings.postId, meetings.category from meetings LEFT OUTER JOIN sharing on meetings.postId=sharing.postId where (meetings.userId =? or sharing.sharedWithId = ?) and cast(dateTime as date) = ?  order by dateTime");

	if(!$check->bind_param('iis', $userId, $userId, $date)){
		echo json_encode(array(
		"success" => false,
		"message" => "failed bind"
		));
		exit;
	}
	
	if(!$check->execute()){
		echo json_encode(array(
		"success" => false,
		"message" => "Couldn't execute"
		));
		exit;
	}
	
	//binds our results and creates some arrays for us
	$check->bind_result($name, $dateTime, $postNum, $categories); 
	$names = array(); 
	$times = array();
	$ids = array();
	$cats = array();
	$ans = array();
	
	//variable for testing
	$q = 0;
	
	//if the process works
	if ($check) {
		//during the fetch process send out all of our arrays
		while($check ->fetch()) {
			$q = $q +1;
			array_push($names,$name);
			array_push($times,$dateTime);
			array_push($ids, $postNum);
			array_push($cats, $categories);
			$ans["mid"] = "fetched x times: ".$q;
		}
		
	$check -> close();
	
	//populate the ans array with values
	$ans["success"] = true;
	$ans["category"] = $cats;
	$ans["message"] = "completed " + $names[0];
	$ans["names"] = $names;
	$ans["postId"] = $ids;
	$ans["times"] = $times[0];
	$ans["user"] = $userId;
	
	//send along our ans array and exit back to JS
	echo json_encode($ans);
	exit; 
	}
	
	//usual failure protocol
	else {
		echo json_encode(array(
	"success" => false,
	"message" => "this should say this"
	));
	exit;
	}
	
?>